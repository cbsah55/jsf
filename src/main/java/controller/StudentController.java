package controller;

import lombok.Data;
import model.Student;
import service.StudentService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@ManagedBean
@RequestScoped
public class StudentController implements Serializable {

    private List<Student> studentList = new ArrayList<>();
    private Map<Long, Student> studentMap = new HashMap<>();
    private Student student = new Student();
    private boolean isEdit = false;
    private String faculty;
    private String programme;
    private final Map<String, Map<String, String>> data = new HashMap<>();
    private Map<String, String> faculties;
    private Map<String, String> programmes;

    @Inject
    private StudentService studentService;

    @PostConstruct
    public void init() {
        faculties = new HashMap<>();
        faculties.put("Science & Technology", "Science & Technology");
        faculties.put("Management", "Management");
        Map<String, String> map = new HashMap<>();
        map.put("BCA", "BCA");
        map.put("B.E. Comp", "B.E. Comp");
        data.put("Science & Technology", map);
        map = new HashMap<>();
        map.put("BBA", "BBA");
        map.put("BBS", "BBS");
        data.put("Management", map);
    }

    public void addInDb(){
        student.setFaculty(this.faculty);
        student.setProgramme(this.programme);
        studentService.addStudent(this.student);
        getAllStudents();
    }
    public void updateInDb(Student std){
        this.isEdit = true;
        this.student = std;
        this.faculty = std.getFaculty();
        this.programme = std.getProgramme();
        student.setFaculty(faculty);
        student.setProgramme(programme);
        studentService.updateStudent(this.student.getId(),this.student);

    }
    public void deleteInDb(long id){
        studentService.deleteStudent(id);

    }
    public void getAllStudents(){
        studentList= studentService.getAllStudents();
    }
    public void add() {
        student.setFaculty(this.faculty);
        student.setProgramme(this.programme);
        long s = studentMap.keySet().size();
        student.setId(s);
        studentMap.put(s, student);
        studentList.clear();
        studentList.addAll(studentMap.values());
        student = new Student();

    }

    public void edit(Student std) {
        this.isEdit = true;
        this.student = std;
        this.faculty = std.getFaculty();
        this.programme = std.getProgramme();
        studentMap.remove(std.getId());
    }

    public void saveEdit(Student student) {
        student.setFaculty(faculty);
        student.setProgramme(programme);
        studentMap.put(student.getId(), student);
        studentList.clear();
        studentList.addAll(studentMap.values());
        this.isEdit = false;
        this.student = new Student();
        this.programmes.clear();
        this.faculties.clear();
    }

    public void delete(long id) {
        studentMap.remove(id);
        studentList.clear();
        studentList.addAll(studentMap.values());

    }

    public void onFacultyChanged() {
        programmes = faculty != null && !faculty.equals("") ? data.get(faculty) : new HashMap<>();
    }

}
