package repository;

import lombok.var;
import model.Student;

import javax.annotation.ManagedBean;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateful
@ManagedBean
public class StudentRepository {
    private static final String PERSISTENCE_UNIT_NAME = "default";
    private static final EntityManager entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME).createEntityManager();
    private static final EntityTransaction transaction = entityManager.getTransaction();

    public void add(Object obj){
        startTransaction();
        entityManager.persist(obj);
        transaction.commit();
    }
    public List findAll(){
        Query queryObj = entityManager.createQuery("SELECT s from Student s");
        List list = queryObj.getResultList();
        return list != null && list.size() > 0 ? list : null;
    }

    public void deleteStudent(long id){
            startTransaction();
        Student student = new Student();
        if (isStudentPresent(id)){
            student.setId(id);
            entityManager.remove(entityManager.merge(student));
        }
        transaction.commit();

    }

    public void updateStudent(long id,Student toUpdateStudent){
        startTransaction();
        if (isStudentPresent(id)){
            Query query = entityManager.createQuery("update Student s set s.id=:id , s.firstName=:fname,s.lastName=:lname,s.middleName=:mname,s.faculty=:faculty,s.programme=:programme");
            query.setParameter("id",toUpdateStudent.getId());
            query.setParameter("fname",toUpdateStudent.getFirstName());
            query.setParameter("lname",toUpdateStudent.getLastName());
            query.setParameter("mname",toUpdateStudent.getMiddleName());
            query.setParameter("faculty",toUpdateStudent.getFaculty());
            query.setParameter("programme",toUpdateStudent.getProgramme());
            int objCount = query.executeUpdate();
            if (objCount>0) System.out.println("Record for " + id + " updated");
        }
        transaction.commit();
    }

    public boolean isStudentPresent(long id){
        Query query = entityManager.createQuery("select s from Student s where s.id = :id");
        query.setParameter("id",id);
        var studentFromDatabase = query.getSingleResult();
        return studentFromDatabase != null;
    }
    public void startTransaction(){
        if (!transaction.isActive()){
            transaction.begin();
        }

    }

}
