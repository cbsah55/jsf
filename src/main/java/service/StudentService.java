package service;

import model.Student;

import java.util.List;

public interface StudentService {
     void addStudent(Student student);
    List<Student> getAllStudents();
    void updateStudent(long id,Student student);
    void deleteStudent(long id);
}
