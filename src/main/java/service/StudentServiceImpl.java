package service;

import model.Student;
import repository.StudentRepository;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
@ManagedBean
public class StudentServiceImpl implements StudentService{

    @Inject
    private StudentRepository studentRepository;

    @Override
    public void addStudent(Student student) {
        studentRepository.add(student);

    }

    @Override
    public List getAllStudents() {
        List studentList = studentRepository.findAll();
        return Optional.ofNullable(studentList).orElse(null);
    }

    @Override
    public void updateStudent(long id,Student student) {
        studentRepository.updateStudent(id,student);
    }

    @Override
    public void deleteStudent(long id) {
        studentRepository.deleteStudent(id);

    }
}
